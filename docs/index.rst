.. libkrylov documentation master file, created by
   sphinx-quickstart on Fri Sep 30 04:51:31 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Libkrylov
=========

`Libkrylov <https://gitlab.com/libkrylov/libkrylov-stable>`_ is a modular
open-source library for extremely large on-the-fly matrix computations using
Krylov subspace methods.

Libkrylov is open-source software and is licensed under the
`3-clause BSD license <https://opensource.org/licenses/BSD-3-Clause>`_.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   Installation Guide <Installation-Guide>
   Background and Notation <Background-and-Notation>
   Quick Start for C/C++ Programmers <Quick-Start-C>
   Quick Start for Fortran Programmers <Quick-Start-Fortran>
   C API <C-API>
   Fortran API <Fortran-API>

